package com.example.scotialogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScotialoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScotialoginApplication.class, args);
	}

}
