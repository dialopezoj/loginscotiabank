package com.example.scotialogin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
	
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	@RequestMapping("/accesoCliente")
	public String accesoCliente() {
		return "accesoCliente";
	}
	
	@RequestMapping("/")
	public String accesoCliente2() {
		return "accesoCliente";
	}
	

}
